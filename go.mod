// Licensed under the MIT license

module gitlab.com/digitale-vertrouwensinfrastructuur/home-service

go 1.16

require (
	github.com/alexflint/go-arg v1.4.1
	github.com/go-chi/chi/v5 v5.0.3
	github.com/go-chi/cors v1.2.0
	github.com/go-chi/render v1.0.1
	github.com/go-playground/validator/v10 v10.6.1
	github.com/golang/mock v1.5.0
	github.com/jinzhu/copier v0.3.0
	github.com/jmoiron/sqlx v1.3.3
	github.com/stretchr/testify v1.7.0
)
