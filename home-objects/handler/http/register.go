// Licensed under the MIT license

package http

import (
	"github.com/go-chi/chi/v5"

	objects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/middleware"
)

func RegisterHTTPEndPoints(router *chi.Mux, repo objects.Repository) {
	h := NewHandler(repo)

	router.Route("/api/v1/objects", func(router chi.Router) {
		router.Use(middleware.Json)
		router.Get("/", h.All)
		router.Post("/", h.Create) // POST /objects
		router.Route("/{objectID}", func(router chi.Router) {
			router.Use(h.HomeObjectContext) // Load the *HomeObject on the request context
			router.Get("/", h.Get)          // GET /objects/123
			router.Delete("/", h.Delete)    // DELETE /objects/123
		})
	})
}
