// Licensed under the MIT license

package http

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	chi "github.com/go-chi/chi/v5"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	objects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects/repository/inmemory"
)

//go:generate mockgen -package mock -source ../../handler.go -destination=../../mock/mock_handler.go
//go:generate mockgen -package mock -source ../../repository.go -destination=../../mock/mock_repository.go

func TestHandler_GetAll(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)
	existingHomeObject := objects.HomeObject{Name: "Dummy", AdditionalInformation: "For testing"}
	o, err := repo.Create(&existingHomeObject)
	assert.NoError(t, err)
	exisitingObjectId := o.HomeObjectID

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/api/v1/objects/", nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotObjectInformation []objects.HomeObjectResponse
	err = json.Unmarshal(body, &gotObjectInformation)
	assert.NoError(t, err)

	respBody := []objects.HomeObject{{
		HomeObjectID:          exisitingObjectId,
		Name:                  "Dummy",
		AdditionalInformation: "For testing",
	}}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody[0].HomeObjectID, gotObjectInformation[0].HomeObjectID)
	assert.Equal(t, respBody[0].Name, gotObjectInformation[0].Name)
	assert.Equal(t, respBody[0].AdditionalInformation, gotObjectInformation[0].AdditionalInformation)
}

func TestHandler_Create(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	requestBody := objects.HomeObject{Name: "Willie Wortel", AdditionalInformation: "Bit confused sometimes"}
	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(requestBody)
	assert.NoError(t, err)
	req, err := http.NewRequest(http.MethodPost, "/api/v1/objects/", b)
	req.Header.Add("Content-Type", "application/json")
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotObjectInformation objects.HomeObjectResponse
	err = json.Unmarshal(body, &gotObjectInformation)
	assert.NoError(t, err)

	respBody := objects.HomeObject{
		Name:                  "Willie Wortel",
		AdditionalInformation: "Bit confused sometimes",
	}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody.Name, gotObjectInformation.Name)
	assert.Equal(t, respBody.AdditionalInformation, gotObjectInformation.AdditionalInformation)
}

func TestHandler_Get(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)
	existingObject := objects.HomeObject{Name: "Dummy", AdditionalInformation: "For testing"}
	o, err := repo.Create(&existingObject)
	assert.NoError(t, err)
	exisitingObjectId := o.HomeObjectID
	exisitingObjectIdStr := strconv.FormatInt(exisitingObjectId, 10)

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/api/v1/objects/"+exisitingObjectIdStr, nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotObjectInformation objects.HomeObjectResponse
	err = json.Unmarshal(body, &gotObjectInformation)
	assert.NoError(t, err)

	respBody := objects.HomeObject{
		HomeObjectID:          exisitingObjectId,
		Name:                  "Dummy",
		AdditionalInformation: "For testing",
	}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody.HomeObjectID, gotObjectInformation.HomeObjectID)
	assert.Equal(t, respBody.Name, gotObjectInformation.Name)
	assert.Equal(t, respBody.AdditionalInformation, gotObjectInformation.AdditionalInformation)
}

func TestHandler_Delete(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)
	existingObject := objects.HomeObject{Name: "Dummy", AdditionalInformation: "For testing"}
	o, err := repo.Create(&existingObject)
	assert.NoError(t, err)
	exisitingObjectId := o.HomeObjectID
	exisitingObjectIdStr := strconv.FormatInt(exisitingObjectId, 10)

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodDelete, "/api/v1/objects/"+exisitingObjectIdStr, nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	_, err = ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.Code)
}
