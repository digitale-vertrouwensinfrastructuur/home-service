// Licensed under the MIT license

package http

import (
	"context"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/go-playground/validator/v10"

	homeObjects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/respond"
)

type Handler struct {
	repository homeObjects.Repository
	validate   *validator.Validate
}

type objectKey string

func NewHandler(repo homeObjects.Repository) *Handler {
	return &Handler{
		repository: repo,
		validate:   validator.New(),
	}
}

// Get all objects
// @Summary Get all objects
// @Description Get all objects.
// @Accept json
// @Produce json
// @Success 200 {object} models.Object
// @Router /api/v1/objects/ [get]
func (h *Handler) All(w http.ResponseWriter, r *http.Request) {
	o, err := h.repository.All()
	if err != nil {
		log.Println("Error occurred while trying to read objects from repository")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	resp, err := homeObjects.NewObjectListResponse(o)
	if err != nil {
		log.Println("Error occurred while trying to create response object")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	respond.Render(w, http.StatusOK, resp)
}

func (h *Handler) HomeObjectContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var object *homeObjects.HomeObject

		objectID, err := strconv.Atoi(chi.URLParam(r, "objectID"))

		if err == nil {
			object, err = h.repository.Read(r.Context(), int64(objectID))
			if err != nil {
				respond.Error(w, http.StatusNotFound, nil)
				return
			}
		} else {
			respond.Error(w, http.StatusNotFound, nil)
			return
		}

		contextObjectKey := objectKey("object")

		ctx := context.WithValue(r.Context(), contextObjectKey, object)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Create an object
// @Summary Create an Object
// @Description Create an object.
// @Accept json
// @Produce json
// @Success 200 {object} models.Object
// @Router /api/v1/objects/ [post]
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	data := &homeObjects.HomeObjectRequest{}
	if err := render.Bind(r, data); err != nil {
		log.Println("Error occurred while trying to read object")
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	givenObject := data.HomeObject

	object, err := h.repository.Create(givenObject)
	if err != nil {
		log.Println("Error occurred while trying to create object")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}

	resp, _ := homeObjects.NewObjectResponse(object)

	respond.Render(w, http.StatusOK, resp)
}

// Get an object by its ID
// @Summary Get an Object
// @Description Get an object by its ID.
// @Accept json
// @Produce json
// @Success 200 {object} models.Object
// @Router /api/v1/objects/{objectID} [get]
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	contextObjectKey := objectKey("object")
	o := r.Context().Value(contextObjectKey).(*homeObjects.HomeObject)

	resp, err := homeObjects.NewObjectResponse(o)
	if err != nil {
		log.Println("Error occurred while trying to read object")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	respond.Render(w, http.StatusOK, resp)
}

// Delete an object by its ID
// @Summary Delete an Object
// @Description Delete an object by its ID.
// @Accept json
// @Produce json
// @Success 200
// @Router /api/v1/objects/{objectID} [delete]
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	objectID, err := strconv.Atoi(chi.URLParam(r, "objectID"))

	if err == nil {
		err = h.repository.Delete(r.Context(), int64(objectID))
		if err != nil {
			respond.Error(w, http.StatusNotFound, nil)
			return
		}
	} else {
		respond.Error(w, http.StatusNotFound, nil)
		return
	}

	respond.Render(w, http.StatusOK, nil)
}
