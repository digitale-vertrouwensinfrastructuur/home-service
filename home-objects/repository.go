// Licensed under the MIT license

package objects

import (
	"context"
)

type Repository interface {
	Create(oi *HomeObject) (*HomeObject, error)
	All() ([]*HomeObject, error)
	Read(ctx context.Context, objectID int64) (*HomeObject, error)
	Delete(ctx context.Context, objectID int64) error
	GetByName(name string) (*HomeObject, error)
}
