// Licensed under the MIT license

package inmemory

import (
	"context"
	"errors"
	"math/rand"
	"time"

	"github.com/jmoiron/sqlx"

	objectModel "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
)

type repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *repository {
	return &repository{db: db}
}

func (r *repository) Create(homeObject *objectModel.HomeObject) (*objectModel.HomeObject, error) {
	rand.Seed(time.Now().UnixNano())
	homeObject.HomeObjectID = int64(rand.Intn(100) + 10)
	homeObjectList = append(homeObjectList, homeObject)

	return homeObject, nil
}

func (r *repository) All() ([]*objectModel.HomeObject, error) {
	return homeObjectList, nil
}

func (r *repository) Read(ctx context.Context, homeObjectID int64) (*objectModel.HomeObject, error) {
	for _, p := range homeObjectList {
		if p.HomeObjectID == homeObjectID {
			return p, nil
		}
	}

	return nil, errors.New("object not found")
}

func (r *repository) Update(ctx context.Context, homeObject *objectModel.HomeObject) error {
	for i, p := range homeObjectList {
		if p.HomeObjectID == homeObject.HomeObjectID {
			homeObjectList[i] = homeObject
			return nil
		}
	}

	return errors.New("object not found")
}

func (r *repository) Delete(ctx context.Context, homeObjectID int64) error {
	for i, p := range homeObjectList {
		if p.HomeObjectID == homeObjectID {
			homeObjectList = append((homeObjectList)[:i], (homeObjectList)[i+1:]...)
			return nil
		}
	}

	return errors.New("object not found")
}

func (r *repository) GetByName(name string) (*objectModel.HomeObject, error) {
	var foundHomeObject *objectModel.HomeObject
	for _, o := range homeObjectList {
		if o.Name == name {
			foundHomeObject = o
		}
	}
	if foundHomeObject == nil {
		return nil, errors.New("object not found")
	}

	return foundHomeObject, nil
}

var homeObjectList = []*objectModel.HomeObject{}
