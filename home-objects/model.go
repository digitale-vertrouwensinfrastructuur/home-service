// Licensed under the MIT license

package objects

import (
	"errors"
	"net/http"

	"github.com/jinzhu/copier"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/filter"
)

type HomeObjectRequest struct {
	*HomeObject
	filter.Filter
}

func (o *HomeObjectRequest) Bind(r *http.Request) error {
	if o.HomeObject == nil {
		return errors.New("missing required Object fields")
	}

	return nil
}

type HomeObjectResponse struct {
	HomeObject
}

type HomeObject struct {
	HomeObjectID          int64  `json:"home_object_id"`
	Name                  string `json:"name" validate:"required"`
	AdditionalInformation string `json:"additional_information"`
}

func NewObjectResponse(homeObject *HomeObject) (HomeObjectResponse, error) {
	var response HomeObjectResponse

	err := copier.Copy(&response, &homeObject)
	if err != nil {
		return response, err
	}

	return response, nil
}

func NewObjectListResponse(homeObjectList []*HomeObject) ([]HomeObjectResponse, error) {
	var responses = make([]HomeObjectResponse, 0)

	for _, p := range homeObjectList {
		var response HomeObjectResponse

		err := copier.Copy(&response, &p)
		if err != nil {
			return responses, err
		}
		responses = append(responses, response)
	}

	return responses, nil
}
