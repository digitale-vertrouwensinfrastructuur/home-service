// Licensed under the MIT license

package server

import (
	objects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	objectsHandler "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects/handler/http"
	objectsRepo "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects/repository/inmemory"
)

func (s *Server) initHomeObjects() {
	s.initObjectsList()
}

func (s *Server) initObjectsList() {
	newObjectsRepo := objectsRepo.New(s.GetDB())
	newObjectsRepo.Create(&objects.HomeObject{Name: "Zuurstoffles", AdditionalInformation: "In de woonkamer"})
	newObjectsRepo.Create(&objects.HomeObject{Name: "Traplift", AdditionalInformation: "Alleen tot de eerste verdieping"})

	objectsHandler.RegisterHTTPEndPoints(s.router, newObjectsRepo)
}
