// Licensed under the MIT license

package server

import (
	"net/http"

	homeObjectsRepo "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects/repository/inmemory"
	occupantInformationHandler "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupantinformation/handler/http"
	occupantsRepo "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants/repository/inmemory"
)

func (s *Server) initOccupantInformation() {
	s.initOccupantInformationList()
}

func (s *Server) initOccupantInformationList() {
	client := &http.Client{
		Timeout: s.cfg.Services.HttpClientTimeout,
	}

	newOccupantsRepo := occupantsRepo.New(s.GetDB())
	newHomeObjectsRepo := homeObjectsRepo.New(s.GetDB())

	occupantInformationHandler.RegisterHTTPEndPoints(s.router, newOccupantsRepo, newHomeObjectsRepo, client, s.cfg.Services.PolicyServiceEndpoint, s.cfg.Services.CalamityServiceEndpoint)
}
