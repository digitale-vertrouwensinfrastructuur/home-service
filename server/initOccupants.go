// Licensed under the MIT license

package server

import (
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
	occupantsHandler "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants/handler/http"
	occupantsRepo "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants/repository/inmemory"
)

func (s *Server) initOccupants() {
	s.initOccupantsList()
}

func (s *Server) initOccupantsList() {
	newOccupantsRepo := occupantsRepo.New(s.GetDB())
	newOccupantsRepo.Create(&occupants.Occupant{Name: "Kwik", AdditionalInformation: "12 jaar oud"})
	newOccupantsRepo.Create(&occupants.Occupant{Name: "Kwek", AdditionalInformation: "16 jaar oud"})

	occupantsHandler.RegisterHTTPEndPoints(s.router, newOccupantsRepo)
}
