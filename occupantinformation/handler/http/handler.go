// Licensed under the MIT license

package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/go-playground/validator/v10"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/calamityinformation"
	homeObjects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupantinformation"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/policy"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/respond"
	restclient "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/restclient"
)

type Handler struct {
	occupantsRepository     occupants.Repository
	objectsRepository       homeObjects.Repository
	httpClient              restclient.HTTPClient
	validate                *validator.Validate
	policyServiceEndpoint   string
	calamityServiceEndpoint string
}

func NewHandler(occupantsRepository occupants.Repository, homeObjectsRepository homeObjects.Repository, client restclient.HTTPClient, policyServiceEndpoint string, calamityServiceEndpoint string) *Handler {
	return &Handler{
		occupantsRepository:     occupantsRepository,
		objectsRepository:       homeObjectsRepository,
		httpClient:              client,
		validate:                validator.New(),
		policyServiceEndpoint:   policyServiceEndpoint,
		calamityServiceEndpoint: calamityServiceEndpoint,
	}
}

// Get a occupantInformation by its address
// @Summary Get a OccupantInformation
// @Description Get a occupantInformation by its address.
// @Accept json
// @Produce json
// @Success 200 {object} models.OccupantInformation
// @Router /api/v1/occupant-information/{occupantInformationID} [get]
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	//TODO: get address from home repository, as this service represents only one home
	address := "Duckstad, Kweklaan 36"

	policies, err := fetchPolicies(h.httpClient, h.policyServiceEndpoint)
	if err != nil {
		errString := fmt.Sprintf("Error occurred while trying to fetch policies on address '%s'", address)
		log.Println(errString)
		respond.Error(w, http.StatusInternalServerError, errString)
	}
	calamityInformation, err := fetchCalamityInformation(address, h.httpClient, h.calamityServiceEndpoint)
	if err != nil {
		errString := fmt.Sprintf("Error occurred while trying to fetch calamity information on address '%s'", address)
		log.Println(errString)
		respond.Error(w, http.StatusInternalServerError, errString)
		return
	}

	valid := containsValidPolicyOfType(policies, "read:occupant-information", calamityInformation)
	if valid {
		oi, err := readOccupantInformation(h.occupantsRepository, h.objectsRepository)
		if err != nil {
			errString := "Error occurred while trying to read occupant information"
			log.Println(errString)
			respond.Error(w, http.StatusInternalServerError, errString)
			return
		}
		respond.Render(w, http.StatusOK, occupantinformation.OccupantInformationResponse{OccupantInformation: *oi})
	} else {
		errString := fmt.Sprintf("No valid policy found to provide occupant information on address '%s'", address)
		log.Println(errString)
		respond.Error(w, http.StatusUnauthorized, errString)
	}
}

func fetchPolicies(client restclient.HTTPClient, policyServiceEndpoint string) (*[]policy.Policy, error) {
	log.Printf("Fetching policy on: %s", policyServiceEndpoint)
	response, err := client.Get(policyServiceEndpoint)
	if err != nil {
		log.Println("Error occurred while fetching policy")
		return nil, err
	}

	defer response.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(response.Body)

	var p []policy.Policy
	err = json.Unmarshal(bodyBytes, &p)
	if err != nil {
		log.Println("Error occurred while unmarshalling policy response")
		return nil, err
	}

	return &p, nil
}

func fetchCalamityInformation(address string, client restclient.HTTPClient, calamityServiceEndpoint string) (*calamityinformation.CalamityInformation, error) {
	calamityInformationService, _ := url.Parse(calamityServiceEndpoint)
	params := url.Values{}
	params.Add("address", address)
	calamityInformationService.RawQuery = params.Encode()

	log.Printf("Fetching calamity information on: %s", calamityInformationService.String())
	response, err := client.Get(calamityInformationService.String())
	if err != nil {
		log.Println("Error occurred while fetching calamity information")
		return nil, err
	}

	defer response.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(response.Body)

	var ci calamityinformation.CalamityInformation
	err = json.Unmarshal(bodyBytes, &ci)
	if err != nil {
		log.Println("Error occurred while unmarshalling calamity information response")
		return nil, err
	}

	return &ci, nil
}

func containsValidPolicyOfType(policies *[]policy.Policy, policyType string, calamityInformation *calamityinformation.CalamityInformation) bool {
	for _, policy := range *policies {
		valid := isValidPolicyOfType(policy, policyType, calamityInformation)
		if valid {
			return true
		}
	}
	return false
}

func isValidPolicyOfType(policy policy.Policy, policyType string, calamityInformation *calamityinformation.CalamityInformation) bool {
	//TODO: policy should be properly typed, so we can use that type here
	if policy.Action.Scope == policyType {
		valid := isValidOccupantInformationPolicy(policy, calamityInformation)
		if valid {
			return true
		} else {
			fmt.Println("Invalid policy of type " + policyType)
			fmt.Printf("%+v\n", policy)
		}
	}
	return false
}

func isValidOccupantInformationPolicy(policy policy.Policy, calamityInformation *calamityinformation.CalamityInformation) bool {
	return (policy.Actor == "bevelvoerder" &&
		policy.Action.Scope == "read:occupant-information" &&
		policy.Actee.Actor == "digitale-huis" &&
		areAllConditionsValid(policy.Conditions, calamityInformation))
}

func areAllConditionsValid(conditions []policy.Condition, calamityInformation *calamityinformation.CalamityInformation) bool {
	for _, condition := range conditions {
		valid := isValidCondition(condition, calamityInformation)
		if !valid {
			return false
		}
	}
	return true
}

func isValidCondition(condition policy.Condition, calamityInformation *calamityinformation.CalamityInformation) bool {
	if condition.Type == "POLICY_NOT_EXPIRED" {
		return true //TODO: implement expirationDate on policy
	} else if condition.Type == "CALAMITY_AT_ADDRESS" && condition.Authority.Actor == "meldkamer" {
		return calamityInformation.Active
	}
	return false
}

func readOccupantInformation(occupantsRepository occupants.Repository, homeObjectsRepository homeObjects.Repository) (*occupantinformation.OccupantInformation, error) {
	occupants, err := occupantsRepository.All()
	if err != nil {
		log.Println("Error occurred while retrieving occupants")
		return nil, err
	}

	homeObjects, err := homeObjectsRepository.All()
	if err != nil {
		log.Println("Error occurred while retrieving objects")
		return nil, err
	}

	return &occupantinformation.OccupantInformation{
		Occupants:   occupants,
		HomeObjects: homeObjects,
	}, nil
}
