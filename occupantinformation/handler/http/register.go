// Licensed under the MIT license

package http

import (
	"github.com/go-chi/chi/v5"

	homeObjects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/middleware"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/restclient"
)

func RegisterHTTPEndPoints(router *chi.Mux, occupantsRepository occupants.Repository, objectsRepository homeObjects.Repository, client restclient.HTTPClient, policyServiceEndpoint string, calamityServiceEndpoint string) {
	h := NewHandler(occupantsRepository, objectsRepository, client, policyServiceEndpoint, calamityServiceEndpoint)

	router.Route("/api/v1/occupant-information", func(router chi.Router) {
		router.Use(middleware.Json)
		router.Get("/", h.Get)
	})
}
