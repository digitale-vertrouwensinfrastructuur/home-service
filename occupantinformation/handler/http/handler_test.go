// Licensed under the MIT license

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	chi "github.com/go-chi/chi/v5"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/calamityinformation"
	objects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	objectsMock "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects/mock"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupantinformation"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
	occupantsMock "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants/mock"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/policy"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/mock"
)

//go:generate mockgen -package mock -source ../../handler.go -destination=../../mock/mock_handler.go
//go:generate mockgen -package mock -source ../../../occupants/repository.go -destination=../../../occupants/mock/mock_repository.go
//go:generate mockgen -package mock -source ../../../home-objects/repository.go -destination=../../../home-objects/mock/mock_repository.go

func TestHandler_Get(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	address := "Duckstad, Kweklaan 36"

	policyRespBody := []policy.Policy{{
		Actor: "bevelvoerder",
		Action: policy.Action{
			Scope:       "read:occupant-information",
			Description: "bewonersinformatie lezen",
		},
		Actee: policy.Datasource{
			Actor: "digitale-huis",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "POLICY_NOT_EXPIRED",
				Authority: policy.Datasource{
					Actor: "any-authority",
					Url:   "http://localhost:8088/some",
				},
				Description: "dat de toestemming niet verlopen is",
			},
			{
				Type: "CALAMITY_AT_ADDRESS",
				Authority: policy.Datasource{
					Actor: "meldkamer",
					Url:   "http://localhost:3084/api/v1/calamity",
				},
				Description: "dat er een calamiteit is op het adres",
			},
		},
		Goal: "To test this",
	}}
	policyRespJson, err := json.Marshal(policyRespBody)
	assert.NoError(t, err)
	policyResponseBody := ioutil.NopCloser(bytes.NewReader(policyRespJson))
	policyResponse := http.Response{
		StatusCode: 200,
		Body:       policyResponseBody,
	}

	client := mock.NewMockHTTPClient(ctrl)
	policyRequestUrl := "http://localhost:3085/api/v1/policies"
	client.EXPECT().Get(policyRequestUrl).Return(&policyResponse, nil).AnyTimes()

	clientRespBody := calamityinformation.CalamityInformation{
		CalamityInformationID: 42,
		Address:               address,
		Active:                true,
	}
	clientRespJson, err := json.Marshal(clientRespBody)
	assert.NoError(t, err)
	clientResponseBody := ioutil.NopCloser(bytes.NewReader(clientRespJson))
	clientResponse := http.Response{
		StatusCode: 200,
		Body:       clientResponseBody,
	}

	clientRequestUrl := fmt.Sprintf("http://localhost:3084/api/v1/calamity?address=%s", url.QueryEscape(address))
	client.EXPECT().Get(clientRequestUrl).Return(&clientResponse, nil).AnyTimes()

	repoOccupants := []*occupants.Occupant{{OccupantID: 33, Name: "Dummy", AdditionalInformation: "For testing"}}
	occupantsRepo := occupantsMock.NewMockRepository(ctrl)
	occupantsRepo.EXPECT().All().Return(repoOccupants, nil)

	repoObjects := []*objects.HomeObject{{HomeObjectID: 66, Name: "Dummy object", AdditionalInformation: "For object testing"}}
	objectsRepo := objectsMock.NewMockRepository(ctrl)
	objectsRepo.EXPECT().All().Return(repoObjects, nil)

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, occupantsRepo, objectsRepo, client, "http://localhost:3085/api/v1/policies", "http://localhost:3084/api/v1/calamity")

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/v1/occupant-information/?address=%s", address), nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotOccupantInformation occupantinformation.OccupantInformationResponse
	err = json.Unmarshal(body, &gotOccupantInformation)
	assert.NoError(t, err)

	respBody := occupantinformation.OccupantInformation{
		Occupants: []*occupants.Occupant{
			{OccupantID: 33, Name: "Dummy", AdditionalInformation: "For testing"},
		},
		HomeObjects: []*objects.HomeObject{
			{HomeObjectID: 66, Name: "Dummy object", AdditionalInformation: "For object testing"},
		},
	}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody.Occupants, gotOccupantInformation.Occupants)
	assert.Equal(t, respBody.HomeObjects, gotOccupantInformation.HomeObjects)
}
