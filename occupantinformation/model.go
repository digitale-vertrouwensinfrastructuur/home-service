// Licensed under the MIT license

package occupantinformation

import (
	"github.com/jinzhu/copier"

	objects "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/home-objects"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/filter"
)

type OccupantInformationRequest struct {
	OccupantInformation
	filter.Filter
}

type OccupantInformationResponse struct {
	OccupantInformation
}

type OccupantInformation struct {
	Occupants   []*occupants.Occupant `json:"occupants"`
	HomeObjects []*objects.HomeObject `json:"home_objects"`
}

func NewOccupantInformationResponse(occupantInformation *OccupantInformation) (OccupantInformationResponse, error) {
	var response OccupantInformationResponse

	err := copier.Copy(&response, &occupantInformation)
	if err != nil {
		return response, err
	}

	return response, nil
}

func NewOccupantInformationListResponse(occupantInformationList []*OccupantInformation) ([]OccupantInformationResponse, error) {
	var responses []OccupantInformationResponse

	for _, p := range occupantInformationList {
		var response OccupantInformationResponse

		err := copier.Copy(&response, &p)
		if err != nil {
			return responses, err
		}
		responses = append(responses, response)
	}

	return responses, nil
}
