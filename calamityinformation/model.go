// Licensed under the MIT license

package calamityinformation

import (
	"github.com/jinzhu/copier"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/filter"
)

type CalamityInformationRequest struct {
	CalamityInformation
	filter.Filter
}

type CalamityInformationResponse struct {
	CalamityInformation
}

type CalamityInformation struct {
	CalamityInformationID int64  `json:"calamity_information_id"`
	Address               string `json:"address" validate:"required"`
	Active                bool   `json:"active" validate:"required"`
}

func NewCalamityInformationResponse(calamityInformation *CalamityInformation) (CalamityInformationResponse, error) {
	var response CalamityInformationResponse

	err := copier.Copy(&response, &calamityInformation)
	if err != nil {
		return response, err
	}

	return response, nil
}
