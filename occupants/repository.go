// Licensed under the MIT license

package occupants

import (
	"context"
)

type Repository interface {
	Create(oi *Occupant) (*Occupant, error)
	All() ([]*Occupant, error)
	Read(ctx context.Context, occupantID int64) (*Occupant, error)
	Delete(ctx context.Context, occupantID int64) error
	GetByName(name string) (*Occupant, error)
}
