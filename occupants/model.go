// Licensed under the MIT license

package occupants

import (
	"errors"
	"net/http"

	"github.com/jinzhu/copier"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/filter"
)

type OccupantRequest struct {
	*Occupant
	filter.Filter
}

func (o *OccupantRequest) Bind(r *http.Request) error {
	if o.Occupant == nil {
		return errors.New("missing required Occupant fields")
	}

	return nil
}

type OccupantResponse struct {
	Occupant
}

type Occupant struct {
	OccupantID            int64  `json:"occupant_id"`
	Name                  string `json:"name" validate:"required"`
	AdditionalInformation string `json:"additional_information"`
}

func NewOccupantResponse(occupant *Occupant) (OccupantResponse, error) {
	var response OccupantResponse

	err := copier.Copy(&response, &occupant)
	if err != nil {
		return response, err
	}

	return response, nil
}

func NewOccupantListResponse(occupantList []*Occupant) ([]OccupantResponse, error) {
	var responses = make([]OccupantResponse, 0)

	for _, p := range occupantList {
		var response OccupantResponse

		err := copier.Copy(&response, &p)
		if err != nil {
			return responses, err
		}
		responses = append(responses, response)
	}

	return responses, nil
}
