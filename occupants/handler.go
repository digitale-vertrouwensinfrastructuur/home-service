// Licensed under the MIT license

package occupants

import "net/http"

type HTTP interface {
	OccupantContext(next http.Handler)
	Create(w http.ResponseWriter, r *http.Request)
	All(w http.ResponseWriter, r *http.Request)
	Get(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}
