// Licensed under the MIT license

package http

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	chi "github.com/go-chi/chi/v5"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants/repository/inmemory"
)

//go:generate mockgen -package mock -source ../../handler.go -destination=../../mock/mock_handler.go

func TestHandler_GetAll(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)
	existingOccupant := occupants.Occupant{Name: "Dummy", AdditionalInformation: "For testing"}
	o, err := repo.Create(&existingOccupant)
	assert.NoError(t, err)
	exisitingOccupantId := o.OccupantID

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/api/v1/occupants/", nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotOccupantInformation []occupants.OccupantResponse
	err = json.Unmarshal(body, &gotOccupantInformation)
	assert.NoError(t, err)

	respBody := []occupants.Occupant{{
		OccupantID:            exisitingOccupantId,
		Name:                  "Dummy",
		AdditionalInformation: "For testing",
	}}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody[0].OccupantID, gotOccupantInformation[0].OccupantID)
	assert.Equal(t, respBody[0].Name, gotOccupantInformation[0].Name)
	assert.Equal(t, respBody[0].AdditionalInformation, gotOccupantInformation[0].AdditionalInformation)
}

func TestHandler_Create(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	requestBody := occupants.Occupant{Name: "Willie Wortel", AdditionalInformation: "Bit confused sometimes"}
	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(requestBody)
	assert.NoError(t, err)
	req, err := http.NewRequest(http.MethodPost, "/api/v1/occupants/", b)
	req.Header.Add("Content-Type", "application/json")
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotOccupantInformation occupants.OccupantResponse
	err = json.Unmarshal(body, &gotOccupantInformation)
	assert.NoError(t, err)

	respBody := occupants.Occupant{
		Name:                  "Willie Wortel",
		AdditionalInformation: "Bit confused sometimes",
	}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody.Name, gotOccupantInformation.Name)
	assert.Equal(t, respBody.AdditionalInformation, gotOccupantInformation.AdditionalInformation)
}

func TestHandler_Get(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)
	existingOccupant := occupants.Occupant{Name: "Dummy", AdditionalInformation: "For testing"}
	o, err := repo.Create(&existingOccupant)
	assert.NoError(t, err)
	exisitingOccupantId := o.OccupantID
	exisitingOccupantIdStr := strconv.FormatInt(exisitingOccupantId, 10)

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/api/v1/occupants/"+exisitingOccupantIdStr, nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotOccupantInformation occupants.OccupantResponse
	err = json.Unmarshal(body, &gotOccupantInformation)
	assert.NoError(t, err)

	respBody := occupants.Occupant{
		OccupantID:            exisitingOccupantId,
		Name:                  "Dummy",
		AdditionalInformation: "For testing",
	}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody.OccupantID, gotOccupantInformation.OccupantID)
	assert.Equal(t, respBody.Name, gotOccupantInformation.Name)
	assert.Equal(t, respBody.AdditionalInformation, gotOccupantInformation.AdditionalInformation)
}

func TestHandler_Delete(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := inmemory.New(nil)
	existingOccupant := occupants.Occupant{Name: "Dummy", AdditionalInformation: "For testing"}
	o, _ := repo.Create(&existingOccupant)
	exisitingOccupantId := strconv.FormatInt(o.OccupantID, 10)

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodDelete, "/api/v1/occupants/"+exisitingOccupantId, nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	_, err = ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.Code)
}
