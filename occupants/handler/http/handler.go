// Licensed under the MIT license

package http

import (
	"context"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/go-playground/validator/v10"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/utility/respond"
)

type Handler struct {
	repository occupants.Repository
	validate   *validator.Validate
}

type occupantKey string

func NewHandler(repo occupants.Repository) *Handler {
	return &Handler{
		repository: repo,
		validate:   nil,
	}
}

// Get all occupants
// @Summary Get all occupants
// @Description Get all occupants.
// @Accept json
// @Produce json
// @Success 200 {object} models.Occupant
// @Router /api/v1/occupants/ [get]
func (h *Handler) All(w http.ResponseWriter, r *http.Request) {
	o, err := h.repository.All()
	if err != nil {
		log.Println("Error occurred while trying to read occupants")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	resp, err := occupants.NewOccupantListResponse(o)
	if err != nil {
		log.Println("Error occurred while trying to read occupants")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	respond.Render(w, http.StatusOK, resp)
}

func (h *Handler) OccupantContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var occupant *occupants.Occupant

		occupantID, err := strconv.Atoi(chi.URLParam(r, "occupantID"))

		if err == nil {
			occupant, err = h.repository.Read(r.Context(), int64(occupantID))
			if err != nil {
				respond.Error(w, http.StatusNotFound, nil)
				return
			}
		} else {
			respond.Error(w, http.StatusNotFound, nil)
			return
		}

		contextOccupantKey := occupantKey("occupant")

		ctx := context.WithValue(r.Context(), contextOccupantKey, occupant)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Create an occupant
// @Summary Create an Occupant
// @Description Create an occupant.
// @Accept json
// @Produce json
// @Success 200 {object} models.Occupant
// @Router /api/v1/occupants/ [post]
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	data := &occupants.OccupantRequest{}
	if err := render.Bind(r, data); err != nil {
		log.Println("Error occurred while trying to read occupant")
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	givenOccupant := data.Occupant

	occupant, err := h.repository.Create(givenOccupant)
	if err != nil {
		log.Println("Error occurred while trying to create occupant")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}

	resp, _ := occupants.NewOccupantResponse(occupant)

	respond.Render(w, http.StatusOK, resp)
}

// Get an occupant by its ID
// @Summary Get an Occupant
// @Description Get an occupant by its ID.
// @Accept json
// @Produce json
// @Success 200 {object} models.Occupant
// @Router /api/v1/occupants/{occupantID} [get]
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	contextOccupantKey := occupantKey("occupant")
	o := r.Context().Value(contextOccupantKey).(*occupants.Occupant)

	resp, err := occupants.NewOccupantResponse(o)
	if err != nil {
		log.Println("Error occurred while trying to read occupant")
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	respond.Render(w, http.StatusOK, resp)
}

// Delete an occupant by its ID
// @Summary Delete an Occupant
// @Description Delete an occupant by its ID.
// @Accept json
// @Produce json
// @Success 200
// @Router /api/v1/occupants/{occupantID} [delete]
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	occupantID, err := strconv.Atoi(chi.URLParam(r, "occupantID"))

	if err == nil {
		err = h.repository.Delete(r.Context(), int64(occupantID))
		if err != nil {
			respond.Error(w, http.StatusNotFound, nil)
			return
		}
	} else {
		respond.Error(w, http.StatusNotFound, nil)
		return
	}

	respond.Render(w, http.StatusOK, nil)
}
