// Licensed under the MIT license

package http

import (
	"github.com/go-chi/chi/v5"

	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/middleware"
	"gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
)

func RegisterHTTPEndPoints(router *chi.Mux, repo occupants.Repository) {
	h := NewHandler(repo)

	router.Route("/api/v1/occupants", func(router chi.Router) {
		router.Use(middleware.Json)
		router.Get("/", h.All)
		router.Post("/", h.Create) // POST /occupants
		router.Route("/{occupantID}", func(router chi.Router) {
			router.Use(h.OccupantContext) // Load the *Occupant on the request context
			router.Get("/", h.Get)        // GET /occupants/123
			router.Delete("/", h.Delete)  // DELETE /occupants/123
		})
	})
}
