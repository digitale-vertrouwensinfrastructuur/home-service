// Licensed under the MIT license

package inmemory

import (
	"context"
	"errors"
	"math/rand"
	"time"

	"github.com/jmoiron/sqlx"

	occupantModel "gitlab.com/digitale-vertrouwensinfrastructuur/home-service/occupants"
)

type repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *repository {
	return &repository{db: db}
}

func (r *repository) Create(occupant *occupantModel.Occupant) (*occupantModel.Occupant, error) {
	rand.Seed(time.Now().UnixNano())
	occupant.OccupantID = int64(rand.Intn(100) + 10)
	occupantList = append(occupantList, occupant)

	return occupant, nil
}

func (r *repository) All() ([]*occupantModel.Occupant, error) {
	return occupantList, nil
}

func (r *repository) Read(ctx context.Context, occupantID int64) (*occupantModel.Occupant, error) {
	for _, o := range occupantList {
		if o.OccupantID == occupantID {
			return o, nil
		}
	}

	return nil, errors.New("occupant not found")
}

func (r *repository) Update(ctx context.Context, occupant *occupantModel.Occupant) error {
	for i, o := range occupantList {
		if o.OccupantID == occupant.OccupantID {
			occupantList[i] = occupant
			return nil
		}
	}

	return errors.New("occupant not found")
}

func (r *repository) Delete(ctx context.Context, occupantID int64) error {
	for i, o := range occupantList {
		if o.OccupantID == occupantID {
			occupantList = append((occupantList)[:i], (occupantList)[i+1:]...)
			return nil
		}
	}

	return errors.New("occupant not found")
}

func (r *repository) GetByName(name string) (*occupantModel.Occupant, error) {
	var foundOccupant *occupantModel.Occupant
	for _, o := range occupantList {
		if o.Name == name {
			foundOccupant = o
		}
	}
	if foundOccupant == nil {
		return nil, errors.New("occupant not found")
	}

	return foundOccupant, nil
}

var occupantList = []*occupantModel.Occupant{}
